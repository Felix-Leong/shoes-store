import Vue from "vue";
import Vuex from "vuex";
import cart from "@/store/modules/cart";
import user from "@/store/modules/user";

Vue.use(Vuex);

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== "production",
  state: {},
  mutations: {},
  actions: {},
  getters: {},
  modules: {
    cart,
    user
  }
});
