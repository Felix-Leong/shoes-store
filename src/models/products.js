export default [
  {
    id: 1,
    image: "/images/01.jpeg",
    name: "Shoes 01",
    desc:
      "Does the complicate analyst scroll the name? Any audible stunt crosses the downstairs. A classic rock puzzles a model. A killer flutes an attended software. A contemporary moon offends. ",
    price: 100
  },
  {
    id: 2,
    image: "/images/02.jpeg",
    name: "Shoes 02",
    desc:
      "A family barks on top of the asset! Why does the wound count a catalog? The choir rocks. The defeat nests a textbook urge next to a flame. My continental counts a memorable suicide.",
    price: 120
  },
  {
    id: 3,
    image: "/images/03.jpeg",
    name: "Shoes 03",
    desc:
      "A family barks on top of the asset! Why does the wound count a catalog? The choir rocks. The defeat nests a textbook urge next to a flame. My continental counts a memorable suicide.",
    price: 130
  },
  {
    id: 4,
    image: "/images/04.jpeg",
    name: "Shoes 04",
    desc:
      "A family barks on top of the asset! Why does the wound count a catalog? The choir rocks. The defeat nests a textbook urge next to a flame. My continental counts a memorable suicide.",
    price: 140
  },
  {
    id: 5,
    image: "/images/05.jpeg",
    name: "Shoes 05",
    desc:
      "A family barks on top of the asset! Why does the wound count a catalog? The choir rocks. The defeat nests a textbook urge next to a flame. My continental counts a memorable suicide.",
    price: 160
  },
  {
    id: 6,
    image: "/images/06.jpeg",
    name: "Shoes 06",
    desc:
      "A family barks on top of the asset! Why does the wound count a catalog? The choir rocks. The defeat nests a textbook urge next to a flame. My continental counts a memorable suicide.",
    price: 180
  }
];
