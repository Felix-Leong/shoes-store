import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import i18n from "./i18n";
import Buefy from "buefy";
import SimpleVueValidation from "simple-vue-validator";
import Meta from "vue-meta";

Vue.config.productionTip = false;
Vue.use(Buefy);
Vue.use(Meta);

Vue.use(SimpleVueValidation);
SimpleVueValidation.setMode("manual");

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
